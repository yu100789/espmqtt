#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include "esp_wifi.h"
#include "esp_wpa2.h"
#include "esp_system.h"
#include "tcpip_adapter.h"
#include "nvs_flash.h"
#include "esp_event_loop.h"
#include "esp_smartconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"
#include "driver/uart.h"

uint8_t crc_high_first(uint8_t * ptr, int len);

#define ECHO_TEST_TXD  (GPIO_NUM_4)
#define ECHO_TEST_RXD  (GPIO_NUM_5)
#define ECHO_TEST_RTS  (UART_PIN_NO_CHANGE)
#define ECHO_TEST_CTS  (UART_PIN_NO_CHANGE)
#define BUF_SIZE (1024)

esp_mqtt_client_handle_t user_client;
TaskHandle_t wificount_handle = NULL;
TaskHandle_t smartconfig_example_task_handle = NULL;
wifi_config_t *wifi_config;
uint8_t user_temp=0,status=0,errorcode=0x00,statusheater=0x80;
uint16_t water_volume=0,fan_rpm=0;
uint8_t *data;
uint8_t buffer_set[9]={0x55,0xB8,0,0xAA,0xAA,0xAA,0xAA,0,0xAA};
uint8_t lock=0;
float outtemp=0,intemp=0,water_yield=0;
char json[300];
char *ip;

void smartconfig_example_task(void * parm);
void wificount(void * parm);
static void echo_task(void * parm);

static const char *TAG = "MQTT_SAMPLE";

static EventGroupHandle_t wifi_event_group;
const static int CONNECTED_BIT = BIT0;
static const int ESPTOUCH_DONE_BIT = BIT1;

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
    esp_mqtt_client_handle_t client = event->client;
    user_client=client;
    int msg_id;
    int water_str;
    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
/*            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            msg_id = esp_mqtt_client_subscribe(client, "/topic/qos0", 0);
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
*/
            xTaskCreate(echo_task, "uart_echo_task", 4096, NULL, 10, NULL);
            msg_id = esp_mqtt_client_subscribe(client, "api/v1/1/rules/#", 2);
            ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

/*          msg_id = esp_mqtt_client_unsubscribe(client, "/topic/qos1");
            ESP_LOGI(TAG, "sent unsubscribe successful, msg_id=%d", msg_id);
            break;
*/
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            msg_id = esp_mqtt_client_publish(client, "/topic/qos0", "data", 0, 0, 0);
            ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
            printf("DATA=%.*s\r\n", event->data_len, event->data);
            if(strcmp(event->topic,"api/v1/1/rules/water") == 0 || strcmp(event->topic,"api/v1/1/rules/temperature") == 0){
                if(strcmp(event->topic,"api/v1/1/rules/water") == 0){
                    buffer_set[1]=0xB9;
                    water_str = atoi(event->data);
                    buffer_set[2] = water_str >>8;
                    buffer_set[3] = water_str;
                    buffer_set[7] = crc_high_first(buffer_set, 7);
                    uart_write_bytes(UART_NUM_1, (const char *) buffer_set, 9);
                }
                else if(strcmp(event->topic,"api/v1/1/rules/temperature") == 0){
                    buffer_set[1]=0xB8;
                    water_str = atoi(event->data);
                    buffer_set[2] = water_str;
                    buffer_set[3] = 0xAA;
                    buffer_set[7] = crc_high_first(buffer_set, 7);
                    uart_write_bytes(UART_NUM_1, (const char *) buffer_set, 9);
                }
                lock=1;
                uint8_t i=0;
                while(i<81){
                    vTaskDelay(2);
                    if(lock==0){
                        buffer_set[1]=0xC2;
                        buffer_set[7] = crc_high_first(buffer_set, 7);
                        uart_write_bytes(UART_NUM_1, (const char *) buffer_set, 9);    
                        break;
                    }
                    if(i==26 || i==53 || i==80){
                        uart_write_bytes(UART_NUM_1, (const char *) buffer_set, 9);
                    }
                    i++;
                }
            }
            lock=0;
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
    }
    return ESP_OK;
}

static esp_err_t wifi_event_handler(void *ctx, system_event_t *event)
{
    switch (event->event_id) {
        case SYSTEM_EVENT_STA_START:
            esp_wifi_connect();
            xTaskCreate(wificount, "wificount", 4096, 0, 3, &wificount_handle);
            break;
        case SYSTEM_EVENT_STA_GOT_IP:
            xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
            if(wificount_handle != NULL )
                {
                    vTaskDelete(wificount_handle);
                    wificount_handle = NULL;
                }
            ip = ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip);
            break;
        case SYSTEM_EVENT_STA_DISCONNECTED:
            esp_wifi_connect();
            xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
            if(wificount_handle == NULL)
                {
                    xTaskCreate(wificount, "wificount", 4096, 0, 3, &wificount_handle);
                }
            break;
        default:
            break;
    }
    return ESP_OK;
}

static void wifi_init(void)
{
    tcpip_adapter_init();
    wifi_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK(esp_event_loop_init(wifi_event_handler, NULL));
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    /*ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = CONFIG_WIFI_SSID,
            .password = CONFIG_WIFI_PASSWORD,
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_LOGI(TAG, "start the WIFI SSID:[%s] password:[%s]", CONFIG_WIFI_SSID, "******");
    */
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_LOGI(TAG, "Waiting for wifi");
    xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true, portMAX_DELAY);
}

static void mqtt_app_start(void)
{
    const esp_mqtt_client_config_t mqtt_cfg = {
        //.uri = "mqtt://iot.eclipse.org",
        //.host = "54.169.168.27",
        .host = "54.95.211.233",
        .port = 1883,
        .keepalive = 60,
        .disable_auto_reconnect = false,
        .event_handle = mqtt_event_handler,
        // .user_context = (void *)your_context
    };

    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
    //memcpy(user_client,client,sizeof(client));
    esp_mqtt_client_start(client);
}

void wificount(void * parm)
{
    vTaskDelay(500);
    if(smartconfig_example_task_handle == NULL)
    {
        xTaskCreate(smartconfig_example_task, "smartconfig_example_task", 4096, 0, 3, &smartconfig_example_task_handle);
    }
    //smartconfig_example_task_handle = NULL;
    wificount_handle = NULL ;
    vTaskDelete(NULL);
}

static void sc_callback(smartconfig_status_t status, void *pdata)
{
    switch (status) {
        case SC_STATUS_WAIT:
            ESP_LOGI(TAG, "SC_STATUS_WAIT");
            break;
        case SC_STATUS_FIND_CHANNEL:
            ESP_LOGI(TAG, "SC_STATUS_FINDING_CHANNEL");
            break;
        case SC_STATUS_GETTING_SSID_PSWD:
            ESP_LOGI(TAG, "SC_STATUS_GETTING_SSID_PSWD");
            break;
        case SC_STATUS_LINK:
            ESP_LOGI(TAG, "SC_STATUS_LINK");
            wifi_config = pdata;
            ESP_LOGI(TAG, "SSID:%s", wifi_config->sta.ssid);
            ESP_LOGI(TAG, "PASSWORD:%s", wifi_config->sta.password);
            ESP_ERROR_CHECK( esp_wifi_disconnect() );
            ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_FLASH));
            ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, wifi_config) );
            esp_wifi_set_auto_connect(true);
            ESP_ERROR_CHECK( esp_wifi_connect() );
            break;
        case SC_STATUS_LINK_OVER:
            ESP_LOGI(TAG, "SC_STATUS_LINK_OVER");
            if (pdata != NULL) {
                uint8_t phone_ip[4] = { 0 };
                memcpy(phone_ip, (uint8_t* )pdata, 4);
                ESP_LOGI(TAG, "Phone ip: %d.%d.%d.%d\n", phone_ip[0], phone_ip[1], phone_ip[2], phone_ip[3]);
            }
            xEventGroupSetBits(wifi_event_group, ESPTOUCH_DONE_BIT);
            break;
        default:
            break;
    }
}

void smartconfig_example_task(void * parm)
{
    EventBits_t uxBits;
    ESP_ERROR_CHECK( esp_smartconfig_set_type(SC_TYPE_ESPTOUCH) );
    ESP_ERROR_CHECK( esp_smartconfig_start(sc_callback) );
    while (1) {
        uxBits = xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT | ESPTOUCH_DONE_BIT, true, false, portMAX_DELAY); 
        if(uxBits & CONNECTED_BIT) {
            ESP_LOGI(TAG, "WiFi Connected to ap");
        }
        if(uxBits & ESPTOUCH_DONE_BIT) {
            ESP_LOGI(TAG, "smartconfig over");
            esp_smartconfig_stop();
            vTaskDelete(NULL);
        }
    }
}

void app_main()
{
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

    nvs_flash_init();
    wifi_init();
    mqtt_app_start();
}
uint8_t  crc_high_first(uint8_t * ptr, int len)
{
    uint8_t i; 
    uint8_t crc=0x00;
    while(len--)
    {
        crc ^= *ptr++;
        for (i=8; i>0; --i)
        {
            if (crc & 0x80)
                crc = (crc << 1) ^ 0x13;
            else
                crc = (crc << 1);
        }
    }
    return (crc);
}
static void echo_task(void * parm)
{
    /* Configure parameters of an UART driver,
     * communication pins and install the driver */
    ESP_LOGI(TAG, "Uart initiating...");
    uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity    = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };
    uart_param_config(UART_NUM_1, &uart_config);
    uart_set_pin(UART_NUM_1, ECHO_TEST_TXD, ECHO_TEST_RXD, ECHO_TEST_RTS, ECHO_TEST_CTS);
    uart_driver_install(UART_NUM_1, BUF_SIZE * 2, 0, 0, NULL, 0);
    // Configure a temporary buffer for the incoming data
    //uint8_t *data = (uint8_t *) malloc(BUF_SIZE);
    uint8_t *buf = (uint8_t *) malloc(BUF_SIZE);
    data = (uint8_t *) malloc(BUF_SIZE);
    while (1) {
            // Read data from the UART
            int len = uart_read_bytes(UART_NUM_1, data, BUF_SIZE, 20 / portTICK_RATE_MS);
            // Write data back to the UART
            if(len>0){
                if(data[8]==0xAA){
                    if(data[7]==crc_high_first(data,7)){
                        if(lock == 0){
                            if(data[1]==0xA1 || data[1]==0xA2){
                                if(data[1]==0xA1){
                                    user_temp=data[2];
                                    outtemp=data[3];
                                    outtemp=outtemp/2;
                                    intemp=data[4];
                                    intemp=intemp/2;
                                    errorcode=data[5];
                                    statusheater=data[6];
                                    sprintf(json,"{\"source_ip\":\"%s\",\"auth_token\":\"zX9a2bTLZ-JUMJxHDcux\", \"user_temperature\":\"%d\",\"input_temperature\":\"%.1f\",\"output_temperature\":\"%.1f\",\"error_code\":\"%x\",\"status\":\"%x\"}",ip,user_temp,intemp,outtemp,errorcode,statusheater);
                                    esp_mqtt_client_publish(user_client, "api/v1/devices/3/logs/temperature", json, strlen(json), 0, 0);
                                    ESP_LOGI(TAG, "sent publish successful : %s",json);
                                }
                                else if(data[1]==0xA2){
                                    water_yield=data[2];
                                    water_yield=water_yield/10;
                                    water_volume=data[3]<<8 | data[4];
                                    fan_rpm=data[5]<<8 | data[6];
                                    sprintf(json,"{\"source_ip\":\"%s\",\"auth_token\":\"zX9a2bTLZ-JUMJxHDcux\",\"water_yield\":\"%.1f\",\"water_volume\":\"%d\",\"fan_rpm\":\"%d\"}",ip,water_yield,water_volume,fan_rpm);
                                    esp_mqtt_client_publish(user_client, "api/v1/devices/3/logs/water", json, strlen(json), 0, 0);
                                    ESP_LOGI(TAG, "sent publish successful : %s",json);
                                }
                                data[1]=data[1]+0x10;
                                data[7]=crc_high_first(data,7);
                                uart_write_bytes(UART_NUM_1, (const char *) data, len);
                                memcpy (buf, data,10);
                                uint8_t i=0;
                                while(i<75){
                                    len=uart_read_bytes(UART_NUM_1, data, BUF_SIZE, 20 / portTICK_RATE_MS);
                                    if(len>0){
                                        if(data[8]==0xAA){
                                            if(data[7]==crc_high_first(data,7)){
                                                if(data[1]==0xC1){
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                        i++;
                                    if(i==24 || i==49 || i==74){
                                        uart_write_bytes(UART_NUM_1, (const char *) buf, 9);
                                    }
                                }
                            }
                        }
                        else if(data[1]==0xA8 || data[1]==0xA9){
                            //uart_write_bytes(UART_NUM_1, (const char *) data, 9);
                            lock=0;
                        }
                    }
                    else{
                        data[0]=crc_high_first(data,7);
                        uart_write_bytes(UART_NUM_1, (const char *) data, 1);
                    }  
                data[8]=0;          
                }  
            } 
    }
}